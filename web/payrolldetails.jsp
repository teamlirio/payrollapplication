<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payroll Details</title>
</head>
<body>
Hours Worked: <b>${employee.hoursWorked }</b><br>
Hourly Rate: <b>${employee.payRate }</b><br>
Grosspay: Php <b>${grossPay }</b><br>
Withholding Tax Deduction: Php <b>${withholdingTax}</b><br>
SSS Deduction: Php <b>${sss }</b><br>
HDMF Deduction: Php <b>${hdmf }</b><br>
NetPay: Php <b>${netpay }</b> <br>
<form action="../main.jsp">
<button type="submit"><< GO BACK</button>
</form>
</body>
</html>