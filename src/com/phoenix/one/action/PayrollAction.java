package com.phoenix.one.action;
import com.phoenix.one.model.*;
import com.phoenix.one.process.*;

public class PayrollAction{
	private Employee employee = new Employee();
	private double grossPay;
	private double withholdingTax;
	private double sss;
	private double hdmf;
	private double netpay;
	
	
	public double getSss() {
		return sss;
	}

	public double getHdmf() {
		return hdmf;
	}

	public double getNetpay() {
		return netpay;
	}

	public double getGrossPay() {
		return grossPay;
	}

	public double getWithholdingTax() {
		return withholdingTax;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String execute() {	
		grossPay = ComputeGrossPayProcess.computeGross(employee.getHoursWorked(), employee.getPayRate());
		withholdingTax =  ComputeTax.computeWithholdingTax(grossPay);
		sss = ComputeDeductions.sssDeduction(grossPay);
		hdmf = ComputeDeductions.hmdfDeduction(grossPay);
		netpay = grossPay - withholdingTax - ComputeDeductions.getHmdf() - ComputeDeductions.getSss();
		return "success";
	}
	
	public static void main(String[] args) {
		PayrollAction action = new PayrollAction();
		action.execute();
	}
	
	
}
