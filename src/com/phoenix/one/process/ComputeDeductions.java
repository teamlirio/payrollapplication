package com.phoenix.one.process;

public class ComputeDeductions {
	private static final double SSS = 1000;
	private static final double HMDF = 500;
	
	
	public static double getSss() {
		return SSS;
	}

	public static double getHmdf() {
		return HMDF;
	}

	public static double sssDeduction(double grossPay) {
		grossPay = grossPay - SSS;
		return grossPay;
	}
	
	public static double hmdfDeduction(double grossPay) {
		grossPay = grossPay - HMDF;
		return grossPay;
	}
	
	
}
