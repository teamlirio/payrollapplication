package com.phoenix.one.process;

public class ComputeTax {
	public static double computeWithholdingTax(double grossPay) {
		System.out.println(grossPay);
		if(grossPay < 5000) {
			grossPay = grossPay * .05;
		}
		else if (grossPay < 8000 && grossPay >= 5000) {
			grossPay = grossPay * .07;
		}
		else if(grossPay < 10000 && grossPay >= 8000) {
			grossPay = grossPay * .09;
		}
		else if(grossPay >= 10000) {
			grossPay = grossPay * .15;
		}
		
		return grossPay;
	}
	
}
